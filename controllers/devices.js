const mongoose = require('mongoose');
const Devices = require('../models/devices');
const History = require('../models/history')
const moment = require('moment');

exports.createDevice = function (req, res) {
    console.log(req.query.name);
    const query = { code: req.query.code };
    const update = {
        name: req.query.name,
        code: req.query.code,
        temp: req.query.temp,
        hum: req.query.hum,
        lat: req.query.lat,
        lng: req.query.lng,
        pm2_5:req.query.pm2_5,
        pm10:req.query.pm10,
        address: req.query.address,
        created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
        updated_at: moment().format('YYYY-MM-DD HH:mm:ss'),
        updated_at_millis: moment().valueOf()
    }
    const option = { upsert: true, new: true }
    Devices.findOneAndUpdate(query, update, option)
        .then((newDevices) => {
            History.create(update)
                .then(() => {
                    console.log('Save history')
                    return res.status(200).json({
                        errorCode: '0',
                        message: 'New cause created successfully',
                        data: newDevices,
                    });
                })
                .catch((error) => {
                    console.log(error);
                    return res.status(500).json({
                        errorCode: '200',
                        message: 'Server error. Please try again.',
                        error: error.message,
                    });
                });
        })
        .catch((error) => {
            console.log(error);
            return res.status(500).json({
                errorCode: '200',
                message: 'Server error. Please try again.',
                error: error.message,
            });
        });

}

exports.findAll = function (req, res) {
    console.log(req.query.name);
    return Devices
        .find()
        .select('name code pm2_5 pm10 address hum temp lat lng created_at updated_at')
        .then((devices) => {
            return res.status(200).json({
                errorCode: '0',
                message: 'Selected data success.',
                data: devices,
            });
        })
        .catch((error) => {
            console.log(error);
            res.status(500).json({
                errorCode: '200',
                message: 'Server error. Please try again.',
                error: error.message,
            });
        });
}