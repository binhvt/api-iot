const mongoose = require('mongoose');
const History = require('../models/history')
const moment = require('moment');

exports.getAll = function (req, res) {
    console.log(req.query.name);
    return History
        .find()
        .select('name code pm2_5 pm10 address hum temp lat lng created_at updated_at')
        .then((devices) => {
            return res.status(200).json({
                errorCode: '0',
                message: 'Selected data success.',
                data: devices,
            });
        })
        .catch((error) => {
            console.log(error);
            res.status(500).json({
                errorCode: '200',
                message: 'Server error. Please try again.',
                error: error.message,
            });
        });
}

exports.getByCode = function (req, res) {
    console.log('code: ' + req.query.code + ' from: ' + req.query.from + ' to: ' + req.query.to);
    const from = req.query.from;
    const to = req.query.to
    var query = { code: req.query.code };
    const sort = { updated_at: 'desc' };
    if (from && to) {
        query.updated_at_millis = { $gte: from, $lte: to };
    }

    return History
        .find(query)
        .sort(sort)
        .select('name code pm2_5 pm10 address hum temp lat lng created_at updated_at')
        .then((devices) => {
            return res.status(200).json({
                errorCode: '0',
                message: 'Selected data success.',
                data: devices,
            });
        })
        .catch((error) => {
            console.log(error);
            res.status(500).json({
                errorCode: '200',
                message: 'Server error. Please try again.',
                error: error.message,
            });
        });
}