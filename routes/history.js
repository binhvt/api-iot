var express = require('express');
var router = express.Router();
var historyController = require('../controllers/history')

router.get('/getAll', historyController.getAll);
router.get('/getByCode', historyController.getByCode)

module.exports = router;