var express = require('express');
var router = express.Router();
var deviceController = require('../controllers/devices')

router.get('/getAll', deviceController.findAll);

router.get('/update', deviceController.createDevice)

module.exports = router;