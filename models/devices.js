const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

const devicesSchema = new mongoose.Schema({
    lng: {
        type: String
    },
    lat: {
        type: String
    },
    pm2_5: {
        type: Number
    },
    pm10: {
        type: Number
    },
    temp: {
        type: Number
    },
    hum: {
        type: Number
    },
    name: {
        type: String
    },
    code: {
        type: String
    },
    address: {
        type: String
    },
    created_at: {
        type: String
    },
    updated_at: {
        type: String
    },
    updated_at_millis: {
        type: Number
    }
});

module.exports = mongoose.model('Devices', devicesSchema);